Meneghisso & Pasquotto

[x] Testar Ferramentas de Scaffold
[x] Clonar Framework laravel/laravel 5.4
[x] Provisionar ambiente com Docker

=========================

[ ] Substituir fontes por versões finais
[x] Ajustar tamanhos de corte de imagens
[x] Responsivo do carousel de projetos
[x] Ajustar estilos do CKE (negrito, italico e link - perfil e contato)

[x] Build prod
[x] Dump mysql
[x] preparar .env

Template
[x] Ajustar assets no fluxo do gulp
[x] Incluir fontes
[x] Preparas classes de fonte, responsivo e variaveis de cor
[x] Header
[x] Menu
[x] SubMenu
[x] Footer


Seção: HOME
[x] Layout responsivo
[x] Slideshow Fullscreen

Seção: PERFIL
[x] Layout responsivo
[x] Layout 2 colunas

Seção: PROJETOS
[x] Layout responsivo
[x] Submenu
[x] Listagem
[x] Detalhes
[x] Carrossel de imagens
[x] Ampliação com loader

Seção: CLIPPINGS
[x] Layout responsivo
[x] Listagem
[x] Lightbox

Seção: CONTATO
[x] Layout responsivo
[x] Form de Contato
[x] Envio


=========================

[x] Admin
	[x] Auth
	[x] Usuários CRUD
		[x] Nome
		[x] E-mail
		[x] Senha

[x] Template
	[x] Menu
	[x] Footer

[x] Seção: HOME
	[x] Slideshow Fullscreen
	[x] Banners CRUD
		[x] Imagem
		[x] Ordem

[x] Seção: PERFIL
	[x] Layout 2 colunas
	[x] Escritorio CRUD
		[x] Título
		[x] Texto
		[x] Imagem
	[x] Arquitetos CRUD
		[x] Imagem
		[x] Nome
		[x] Texto
		[x] Ordem

[x] Seção: PROJETOS
	[x] Submenu
	[x] Listagem
	[x] Detalhes
		[x] Carrossel de imagens
	[x] Categorias CRUD (Residenciais, Comerciais, Corporativos)
		[x] Título
		[x] Slug
		[x] Ordem		
	[x] Projetos CRUD
		[x] Categoria
		[x] Local
		[x] Título
		[x] Metragem
		[x] Thumbnail
		[x] Ordem
	[X] Projetos Imagens CRUD
		[X] Imagem
		[X] Ordem

[x] Seção: CLIPPINGS
	[x] Listagem
	[x] Lightbox
	[x] Clippings CRUD
		[x] Título
		[x] Capa
		[x] Revista
		[x] Data (mês/ano)
	[x] Clippings Imagens CRUD
		[x] Imagem
		[x] Ordem

Seção: CONTATO
	[x] Envio de contato
	[x] Contatos Recebidos RD
		[x] Listagem
		[x] Detalhes
		[x] Exportação CSV
	[x] Informações de Contato CRUD
		[x] Texto
		[x] E-mail de contato
		[x] Código Analytics
		[x] Facebook
		[x] Instagram
		[x] Código de Incorporação do Google Maps
