-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: meneghisso_e_pasquotto
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (6,'modern-architecture-wallpaper-2560x1600.jpg',1,'2017-10-15 18:29:36','2017-10-15 18:29:36'),(7,'lescolsrestaurantmarquee-3.jpg',2,'2017-10-15 18:29:40','2017-10-15 18:29:40'),(8,'41161-campus-der-wirtschaftsuniversitaet-19to1.jpeg',3,'2017-10-15 18:29:47','2017-10-15 18:29:47');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (4,'Residenciais','residenciais',0,NULL,NULL),(5,'Comerciais','comerciais',1,NULL,NULL),(6,'Corporativos','corporativos',2,NULL,NULL);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clippings`
--

DROP TABLE IF EXISTS `clippings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clippings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revista` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clippings`
--

LOCK TABLES `clippings` WRITE;
/*!40000 ALTER TABLE `clippings` DISABLE KEYS */;
INSERT INTO `clippings` VALUES (3,'Matéria 1','cities-59.jpg','Revista Teste','2017-02-01','2017-10-16 00:17:47','2017-10-16 00:17:47'),(4,'Outra Matéria','cities-46.jpg','Trip','2017-06-01','2017-10-16 00:18:05','2017-10-16 00:18:05'),(5,'Revista X','forest-6.jpg','X','2017-01-01','2017-10-16 00:18:21','2017-10-16 00:18:21');
/*!40000 ALTER TABLE `clippings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clippings_imagens`
--

DROP TABLE IF EXISTS `clippings_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clippings_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clipping` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clippings_imagens_clipping_foreign` (`clipping`),
  CONSTRAINT `clippings_imagens_clipping_foreign` FOREIGN KEY (`clipping`) REFERENCES `clippings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clippings_imagens`
--

LOCK TABLES `clippings_imagens` WRITE;
/*!40000 ALTER TABLE `clippings_imagens` DISABLE KEYS */;
INSERT INTO `clippings_imagens` VALUES (6,3,'20171015225246home.jpg',0,'2017-10-16 00:52:47','2017-10-16 00:52:47'),(7,3,'20171015225246forest.jpg',1,'2017-10-16 00:52:48','2017-10-16 00:52:48'),(8,3,'20171015225246lights-1.png',2,'2017-10-16 00:52:48','2017-10-16 00:52:48'),(9,3,'20171015225246flowers-4.png',3,'2017-10-16 00:52:48','2017-10-16 00:52:48'),(10,3,'20171015225246flowers-14.png',4,'2017-10-16 00:52:48','2017-10-16 00:52:48'),(11,3,'20171015225246owl-1.png',5,'2017-10-16 00:52:48','2017-10-16 00:52:48'),(12,4,'20171015231242pgskcrrpo5nwzcvhpxg2l8dxtou.jpg',0,'2017-10-16 01:12:44','2017-10-16 01:12:44'),(13,4,'20171015231242uqxbc98ol0i68ywfjokkgq8ih8d.jpg',1,'2017-10-16 01:12:44','2017-10-16 01:12:44'),(14,4,'20171015231242xedb5ghyh19g7svbb9hl40buwbx.jpg',2,'2017-10-16 01:12:44','2017-10-16 01:12:44'),(15,5,'20171015231325road-11.png',0,'2017-10-16 01:13:29','2017-10-16 01:13:29');
/*!40000 ALTER TABLE `clippings_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8mb4_unicode_ci,
  `email_contato` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `instagram` text COLLATE utf8mb4_unicode_ci,
  `google_maps` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'<p>Escrit&oacute;rio 1:<br />\r\nRua Jos&eacute; Felix de Oliveira. 834, Sala 5b<br />\r\nGranja Viana - SP<br />\r\n+55 (11) 4702 3109</p>\r\n\r\n<p>Escrit&oacute;rio 2:<br />\r\nRua Milton Monzoni Wagner, 193, Campolim<br />\r\nSorocaba - SP<br />\r\n+55 (15) 4141 3986</p>','contato@pasquottoarquitetura.com.br','UA-000000-0','http://www.facebook.com/teste','http://www.instagram.com/teste','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.481464135661!2d-46.84110998502164!3d-23.58705988467012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cfaaa468ac405b%3A0x6166313df7b825f3!2sR.+Jos%C3%A9+F%C3%A9lix+de+Oliveira%2C+834+-+Vila+Santo+Ant%C3%B4nio%2C+Cotia+-+SP!5e0!3m2!1spt-BR!2sbr!4v1508116929099\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,'2017-10-16 01:50:17');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
INSERT INTO `contatos_recebidos` VALUES (5,'Bruno Teste','teste@email.com','Monotonectally matrix visionary customer service for competitive ideas. Distinctively scale long-term high-impact human capital without cutting-edge sources.','2017-10-16 04:22:12','2017-10-16 04:22:12');
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2017_10_13_045453_create_banners_table',2),(3,'2017_10_13_045630_create_perfil_escritorio_table',2),(4,'2017_10_13_045643_create_perfil_arquitetos_table',2),(5,'2017_10_13_045659_create_categorias_table',2),(6,'2017_10_13_045713_create_projetos_table',2),(7,'2017_10_13_045721_create_projetos_imagens_table',2),(8,'2017_10_13_045732_create_clippings_table',2),(9,'2017_10_13_045748_create_clippings_imagens_table',2),(10,'2017_10_13_045807_create_contatos_recebidos_table',2),(11,'2017_10_13_045816_create_contato_table',2),(12,'2017_10_15_183825_alter_projetos_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_arquitetos`
--

DROP TABLE IF EXISTS `perfil_arquitetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_arquitetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_arquitetos`
--

LOCK TABLES `perfil_arquitetos` WRITE;
/*!40000 ALTER TABLE `perfil_arquitetos` DISABLE KEYS */;
INSERT INTO `perfil_arquitetos` VALUES (3,'230x300.png','Alexandre Pasquotto','Objectively parallel task effective leadership skills and error-free technology. Efficiently envisioneer resource-leveling paradigms without covalent convergence.',1,'2017-10-15 19:25:07','2017-10-15 19:26:24'),(4,'230x300.png','Mariana Meneghisso','Uniquely engage resource sucking potentialities with quality experiences. Rapidiously matrix performance based catalysts for change rather than parallel platforms.',2,'2017-10-15 19:25:43','2017-10-15 19:26:30');
/*!40000 ALTER TABLE `perfil_arquitetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_escritorio`
--

DROP TABLE IF EXISTS `perfil_escritorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_escritorio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_escritorio`
--

LOCK TABLES `perfil_escritorio` WRITE;
/*!40000 ALTER TABLE `perfil_escritorio` DISABLE KEYS */;
INSERT INTO `perfil_escritorio` VALUES (1,'Meneghisso & Pasquotto Arquitetura','<p>Uniquely network cost effective interfaces vis-a-vis bleeding-edge internal or &quot;organic&quot; sources. Enthusiastically deploy vertical e-markets whereas prospective imperatives. Credibly negotiate competitive outsourcing rather than accurate systems.</p>\r\n\r\n<p>Compellingly create enterprise ROI whereas extensible e-tailers. Compellingly expedite low-risk high-yield strategic theme areas through bricks-and-clicks web-readiness. Appropriately embrace prospective internal or &quot;organic&quot; sources via team building meta-services.</p>\r\n\r\n<p>Energistically foster long-term high-impact human capital after fully researched information. Efficiently incubate 24/365 scenarios rather than vertical resources.</p>','large.jpg',NULL,'2017-10-15 19:12:36');
/*!40000 ALTER TABLE `perfil_escritorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` int(10) unsigned NOT NULL,
  `local` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metragem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_categoria_foreign` (`categoria`),
  CONSTRAINT `projetos_categoria_foreign` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (8,4,'Granja Viana','QueluzVita','granja-viana-queluzvita','150','large.jpg',1,'2017-10-15 19:42:21','2017-10-15 20:49:35'),(9,4,'Morumbi','Casa Morumbi','morumbi-casa-morumbi','500','cities-56.jpg',2,'2017-10-15 19:44:40','2017-10-15 20:49:43'),(10,4,'São Paulo','Projeto Teste','sao-paulo-projeto-teste','1000','town.png',3,'2017-10-15 19:45:15','2017-10-15 20:53:25'),(11,4,'Quarto Teste','Teste','quarto-teste-teste','250','building-1.png',4,'2017-10-15 20:17:45','2017-10-15 20:53:33'),(12,4,'RJ','Teste','rj-teste','600','window.jpg',5,'2017-10-15 20:18:15','2017-10-16 04:07:28');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_foreign` (`projeto`),
  CONSTRAINT `projetos_imagens_projeto_foreign` FOREIGN KEY (`projeto`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,8,'20171015185755berries-2.jpg',0,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(2,8,'20171015185755beach-2.jpg',1,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(3,8,'20171015185755berries.jpg',2,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(4,8,'20171015185755beach.jpg',3,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(5,8,'20171015185755birds.jpg',4,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(6,8,'20171015185755birds-1.png',5,'2017-10-15 20:57:58','2017-10-15 20:57:58'),(14,9,'20171015220719cities-41.jpg',0,'2017-10-16 00:07:40','2017-10-16 00:07:40'),(15,9,'20171015220719cities-52.jpg',1,'2017-10-16 00:07:40','2017-10-16 00:07:40'),(16,9,'20171015220719cities-56.jpg',2,'2017-10-16 00:07:40','2017-10-16 00:07:40'),(17,9,'20171015220738cities-63.jpg',3,'2017-10-16 00:07:40','2017-10-16 00:07:40'),(18,9,'20171015220738cities-69.jpg',4,'2017-10-16 00:07:40','2017-10-16 00:07:40');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Trupe','admin@trupe.net','$2y$10$rl4iML5o2.dlFJ/NnxdzPOsc8EHRuroKtUWCZRvOkMv2ByXepUGSK','CPgOODJNQn6agTtB1vo3KSznDOvJpAEf7V8u0eHOpGOXA5TGEI9zE1ZIZdlR',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-16  3:00:51
