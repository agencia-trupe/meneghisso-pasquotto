<a href="#" id="toggle-menu">MENU</a>
<nav>
	<ul class="menu">
		<li @if(str_is('site.home', Route::currentRouteName())) class="ativo" @endif>
			<a href="{{URL::route('site.home')}}" title="Home">Home</a>
		</li>
		<li class="comsubmenu @if(str_is('site.perfil*', Route::currentRouteName())) ativo @endif" >
			<a href="#" onclick="return false;" title="Perfil">Perfil</a>
			<ul class="submenu">
				<li>
					<a href="{{URL::route('site.perfil.escritorio')}}"  @if(str_is('site.perfil.escritorio*', Route::currentRouteName())) class="ativo" @endif title="Perfil escritório">Perfil escritório</a>
				</li>
				<li>
					<a href="{{URL::route('site.perfil.arquitetos')}}"  @if(str_is('site.perfil.arquitetos*', Route::currentRouteName())) class="ativo" @endif title="Perfil arquitetos">Perfil arquitetos</a>
				</li>
			</ul>
		</li>
		<li @if(str_is('site.projetos*', Route::currentRouteName())) class="ativo" @endif>
			<a href="{{URL::route('site.projetos')}}" title="Projetos">Projetos</a>
		</li>
		<li @if(str_is('site.clippings', Route::currentRouteName())) class="ativo" @endif>
			<a href="{{URL::route('site.clippings')}}" title="Clipping">Clipping</a>
		</li>
		<li @if(str_is('site.contato', Route::currentRouteName())) class="ativo" @endif>
			<a href="{{URL::route('site.contato')}}" title="Contato">Contato</a>
		</li>
	</ul>
	<ul class="menu-social">
		@if(isset($contato) && $contato->facebook)
			<li>
				<a href="{{$contato->facebook}}" target="_blank" title="Facebook">
					<img src="assets/images/layout/facebook-icone.png" alt="Facebook">
				</a>
			</li>
		@endif
		@if(isset($contato) && $contato->instagram)
			<li>
				<a href="{{$contato->instagram}}" target="_blank" title="Instagram">
					<img src="assets/images/layout/instagram-icone.png" alt="Instagram">
				</a>
			</li>
		@endif
	</ul>
</nav>
