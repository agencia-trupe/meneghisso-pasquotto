<header>
	<div class="centralizar">
		<a href="{{URL::route('site.home')}}" title="Página Inicial" id="link-marca">
			<img src="assets/images/layout/meneghisso-e-pasquotto-arquitetura-v2.png" alt="Meneghisso & Pasquotto Arquitetura">
		</a>

		@include('site.partials.menu')
	</div>

</header>
