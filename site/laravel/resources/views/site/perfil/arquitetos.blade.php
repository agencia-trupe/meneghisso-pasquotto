@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1>PERFIL ARQUITETOS</h1>

		<div class="colunas com-cards">
			@foreach($arquitetos as $texto)
				<div class="coluna">
					<div class="card">
						<div class="card-image">
							<img src="assets/images/perfil_arquitetos/{{$texto->imagem}}" alt="{{ENV('SITE_NAME')}}">
						</div>
						<div class="card-text">
							<h2>{{$texto->nome}}</h2>
							<p>{{$texto->texto}}</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>

	</div>

@stop
