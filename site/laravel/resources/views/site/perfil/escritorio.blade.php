@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1>PERFIL ESCRITÓRIO</h1>

		<div class="colunas">
			<div class="coluna narrow">
				<img src="assets/images/perfil_escritorio/{{$texto->imagem}}" alt="{{ENV('SITE_NAME')}}">
			</div>
			<div class="coluna wide">
				<h2>{{$texto->titulo}}</h2>
				<div class="cke">
					{!! $texto->texto !!}
				</div>
			</div>
		</div>

	</div>

@stop
