@extends('site.template')

@section('conteudo')

	<div class="home-banners cycle-slideshow"
		 data-cycle-slides="> .banner">	
		@foreach($banners as $banner)
			<div class="banner" style="background-image:url('assets/images/banners/{{$banner->imagem}}')"></div>
		@endforeach
	</div>

@stop
