@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1 class="noborder">{{$projeto->local}} - {{$projeto->titulo}}</h1>

		@if($projeto->metragem)
			<p class="metragem">Metragem: {{$projeto->metragem}}m<sup>2</sup></p>
		@endif

		@if(count($projeto->imagens))
			<div class="detalhes-projeto">
				<aside>
					@foreach($projeto->imagens as $k => $image)
						<a href="assets/images/projetos_imagens/{{$image->imagem}}" title="Ampliar" target="_blank" @if($k == 0) class="aberto" @endif>
							<img src="assets/images/projetos_imagens/thumbs-quadradas/{{$image->imagem}}">
						</a>
					@endforeach
				</aside>
				<section>
					<div id="ampliada">
						<img src="assets/images/projetos_imagens/{{$projeto->imagens->first()->imagem}}">
					</div>
				</section>
			</div>
		@else
			<p class="empty">Nenhuma Imagem para este Projeto.</p>
		@endif

	</div>

@stop
