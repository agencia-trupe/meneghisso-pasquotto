@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1>PROJETOS</h1>

		<div class="menu-categorias">
			@foreach ($categorias as $key => $cat)
				<a href="{{URL::route('site.projetos', ['categoria' => $cat->slug])}}" @if($filtro == $cat->id) class="ativo" @endif>{{$cat->titulo}}</a>
			@endforeach
		</div>

		<div class="lista-projetos">
			@forelse($projetos as $projeto)
				<a href="projetos/{{$projeto->slug}}" title="{{$projeto->local}}">
					<div class="thumb-link" style="background-image:url('assets/images/projetos/thumbs/{{$projeto->thumbnail}}')">
						<div class="overlay">
							<h2>{{$projeto->local}}</h2>
							<div class="toggle">
								@if($projeto->titulo) <p>{{$projeto->titulo}}</p> @endif
								@if($projeto->metragem) <p>Metragem: {{$projeto->metragem}}m<sup>2</sup></p> @endif
							</div>
						</div>
					</div>
				</a>
			@empty
				<p class="empty">Nenhum Projeto encontrado.</p>
			@endforelse
		</div>

	</div>

@stop
