@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1 class="center">CLIPPING</h1>
		<h2 class="subtitulo">Matérias em revistas e links de vídeos</h2>

		<div class="lista-clippings">
			@forelse($clippings as $clip)
				<a href="#"
				   title="{{$clip->titulo}}"
				   data-images="{{$clip->imagens->pluck('fullpath')}}">
					<img src="assets/images/clippings/{{$clip->capa}}" alt="{{$clip->titulo}}">
					<div class="overlay">
						<div class="overlay-conteudo">
							<h3>{{$clip->titulo}}</h3>
							@if($clip->revista)
								<p>Revista: {{$clip->revista}}</p>
							@endif
							<p>{{$clip->data}}</p>
						</div>
					</div>
				</a>

			@empty
				<p class="empty">Nenhum Clipping encontrado.</p>
			@endforelse
		</div>

	</div>

@stop
