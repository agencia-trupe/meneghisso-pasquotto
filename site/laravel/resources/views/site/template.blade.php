<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2017 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">


	<title>{{ENV('SITE_NAME')}}</title>
	<base href="{{ base_url() }}">

	<meta name="title" content="{{ENV('SITE_NAME')}}" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="url" content="" />

	<meta property="og:title" content="{{ENV('SITE_NAME')}}" />
	<meta property="og:description" content=""/>
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="{{ENV('SITE_NAME')}}"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content=""/>

	<link href="https://fonts.googleapis.com/css?family=Montserrat:200|Ubuntu:300,400|Nunito:200,400" rel="stylesheet">
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="assets/vendor/jquery.js"> </script>
	<script type="text/javascript" src="assets/js/site.js"></script>

	@include('site.partials.analytics')

</head>
<body>

	<div class="conteudo @if(str_is('site.home', Route::currentRouteName())) conteudo-home @endif">

  		@include('site.partials.header')

  		@yield('conteudo')

  	</div>

  	@include('site.partials.footer')

</body>
</html>
