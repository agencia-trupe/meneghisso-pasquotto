@extends('site.template')

@section('conteudo')

	<div class="internas">

		<h1>CONTATO</h1>

		<div class="layout-tres-colunas">
			<div class="coluna">
				<div class="form">
					<h2>Fale Conosco</h2>
					<form action="{{URL::route('site.contato.enviar')}}" method="post">
						{!! csrf_field() !!}

						@if(session('contato_enviado'))
							<p class="sucesso">Sua mensagem foi enviada com sucesso!</p>
						@endif

						@if($errors->any())
	                  		<p class="erro">{{$errors->first()}}</p>
	                	@endif

						<label for="input-nome">Nome</label>
						<input type="text" name="nome" id="input-nome" required value="{{old('nome')}}">
						<label for="input-email">E-mail</label>
						<input type="email" name="email" id="input-email" required value="{{old('email')}}">
						<label for="input-msg">Mensagem</label>
						<textarea name="mensagem" id="input-mensagem" required>{{old('mensagem')}}</textarea>
						<div class="submit">
							<input type="submit" value="ENVIAR">
						</div>
					</form>
				</div>
			</div>
			<div class="coluna">
				<div class="info">
					<h2>Contato</h2>

					<div class="cke">
						{!! nl2br($contato->texto) !!}
					</div>

					<div class="email">
						<a href="mailto:{{$contato->email_contato}}">{{$contato->email_contato}}</a>
					</div>

					<div class="social">
						@if($contato->facebook)
							<a href="{{$contato->facebook}}" target="_blank" title="Facebook">
								<img src="assets/images/layout/facebook-icone2.png" alt="Facebook">
							</a>
						@endif
						@if($contato->instagram)
							<a href="{{$contato->instagram}}" target="_blank" title="Instagram">
								<img src="assets/images/layout/instagram-icone2.png" alt="Instagram">
							</a>
						@endif
					</div>
				</div>
			</div>
			<div class="coluna">
				<div class="maps">
					{!! embed_maps($contato->google_maps) !!}
					{!! embed_maps($contato->google_maps_alt) !!}
				</div>
			</div>
		</div>

	</div>

@stop
