@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
    <div class="row">
      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        	<h2>Contatos Recebidos</h2>

        	<hr>

      		@include('painel.partials.mensagens')

			<a href="{{URL::route('painel.contatos_recebidos.export')}}" class="btn btn-sm btn-success">Exportar contatos</a>

        	<table class="table table-striped table-bordered table-hover">

          		<thead>
          			<tr>
						<th>Data</th>
          				<th>Nome</th>
              			<th>E-mail</th>
						<th>E-Mensagem</th>
          				<th><span class="glyphicon glyphicon-cog"></span></th>
          			</tr>
        		</thead>

        		<tbody>
          		@foreach ($registros as $registro)

            		<tr class="tr-row">
						<td>{{$registro->created_at->format('d/m/Y H:i')}}</td>
                		<td>{{$registro->nome}}</td>
						<td>{{$registro->email}}</td>
            			<td>{{ str_words(strip_tags($registro->mensagem), 15)  }}</td>
            			<td class="crud-actions">
              				<a href="{{ URL::route('painel.contatos_recebidos.show', $registro->id ) }}" class="btn btn-primary btn-sm">ver</a>

							<form action="{{ URL::route('painel.contatos_recebidos.destroy', $registro->id) }}" method="post">
	  	                    	{!! csrf_field() !!}
	  	                    	<input type="hidden" name="_method" value="DELETE">
	  	                    	<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	  	                  	</form>
            			</td>
            		</tr>

          		@endforeach
        		</tbody>
      		</table>

      	</div>
    </div>
</div>

@endsection
