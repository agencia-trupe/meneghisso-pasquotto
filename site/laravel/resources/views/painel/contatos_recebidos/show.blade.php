@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Visualizar Contato Recebido</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

      		<div class="form-group">
      			<label for="inputNome">Nome</label>
      			<input type="text" name="nome" class="form-control" id="inputNome" value="{{$registro->nome}}" disabled>
      		</div>

			<div class="form-group">
				<label for="inputEmail">Email</label>
				<input type="text" name="email" class="form-control" id="inputEmail" value="{{$registro->email}}" disabled>
			</div>

			<div class="form-group">
				<label for="inputMensagem">Mensagem</label>
				<div class="well">
					{{$registro->mensagem}}
				</div>				
			</div>

		</div>
	</div>

	<a href="{{ URL::route('painel.contatos_recebidos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

</div>

@endsection
