@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Banner</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		</div>

      	<form action="{{ URL::route('painel.banners.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            		<div class="form-group">
            			@if(old('imagem'))
            				Imagem atual<br>
            				<img src="assets/img/banners/{{old('imagem')}}" class="img-responsive"><br>
            			@endif
            			<label for="inputImagem">Imagem</label>
            			<input type="file" class="form-control" id="inputImagem" name="imagem">
            		</div>

  				</div>
  			</div>

			<button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			<a href="{{ URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
