@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Banners</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.banners.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Banner</a>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela='banners'>

          	<thead>
          		<tr>
					<th>Ordenar</th>
          			<th>Imagem</th>
          			<th><span class="glyphicon glyphicon-cog"></span></th>
          		</tr>
        	</thead>
        	<tbody>
          	@foreach ($registros as $registro)
            	<tr class="tr-row" id="row_{{ $registro->id }}">
					<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
	                <td>
						<img src="assets/images/banners/thumbs/{{$registro->imagem}}" alt="img-thumbnail">
	                </td>
	            	<td class="crud-actions">
	              		<a href="{{ URL::route('painel.banners.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	                  	<form action="{{ URL::route('painel.banners.destroy', $registro->id) }}" method="post">
	                    	{!! csrf_field() !!}
	                    	<input type="hidden" name="_method" value="DELETE">
	                    	<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                  	</form>
	            	</td>
            	</tr>
          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
