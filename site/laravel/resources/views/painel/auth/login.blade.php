<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="robots" content="index, nofollow" />
  	<meta name="author" content="Trupe Design" />
  	<meta name="copyright" content="2017 Trupe Design" />
  	<meta name="viewport" content="width=device-width,initial-scale=1">

  	<title>{{ENV('SITE_NAME', '')}} - Painel Administrativo</title>

	<base href="{{ url('/') }}/">
	<script>var BASE = "{{ url('/') }}"</script>

	<link rel="stylesheet" href="assets/css/painel/painel.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery.js"><\/script>')</script>

	<script src='assets/vendor/modernizr.js'></script>

</head>
<body class="body-painel-login">

	<div class="main-painel-login">

		<div class="login">

			<form action="{{ URL::route('painel.auth') }}" method="post">

				{!! csrf_field() !!}

		  		<h3>{{ENV('SITE_NAME', '')}} <small>Painel Administrativo</small></h3>

		  		@if(count($errors) > 0)
			  		@foreach ($errors->all() as $error)
            		<div class="alert alert-danger">{{ $error }}</div>
            		@endforeach
				@endif

				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input type="text" class="form-control" placeholder="E-mail" value="{{ old('email') }}" required name="email" autofocus>
				</div>

				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input type="password" class="form-control" placeholder="Senha" required name="password">
				</div>

				<div class="submit-placeholder">
					<label>
						<input type="checkbox" value="1" name="lembrar"> Lembrar de mim
					</label>
		   			<input type="submit" class="btn btn-primary" value="Login">
				</div>

			</form>

		</div>

	</div>

	<footer>
		<div class="container">
			<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
		</div>
	</footer>

	<script src='assets/vendor/bootstrap.js'></script>

</body>
</html>
