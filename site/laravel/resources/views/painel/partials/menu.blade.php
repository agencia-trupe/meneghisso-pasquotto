<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.dashboard') }}">{{ENV('SITE_NAME')}}</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->








				<li @if(str_is('painel.banners*', Route::currentRouteName())) class='active' @endif><a href='painel/banners' title='Banners'>Banners</a></li>

				<li class="dropdown @if(preg_match('~painel.(perfil*)~', Route::currentRouteName())) active @endif ">

					<a href="painel/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Perfil <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.perfil_escritorio*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/perfil_escritorio' title='Escritório'>Escritório</a>
						</li>
						<li @if(str_is('painel.perfil_arquitetos*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/perfil_arquitetos' title='Arquitetos'>Arquitetos</a>
						</li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.(projetos|categorias*)~', Route::currentRouteName())) active @endif ">

					<a href="painel/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projetos <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.categorias*', Route::currentRouteName())) class='active' @endif><a href='painel/categorias' title='Categorias'>Categorias</a></li>
						<li @if(str_is('painel.projetos*', Route::currentRouteName())) class='active' @endif><a href='painel/projetos' title='Projetos'>Projetos</a></li>
					</ul>
				</li>

				<li @if(str_is('painel.clippings*', Route::currentRouteName())) class='active' @endif><a href='painel/clippings' title='Clippings'>Clippings</a></li>

				<li class="dropdown @if(preg_match('~painel.(contato*)~', Route::currentRouteName())) active @endif ">

					<a href="painel/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contato <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.contato.index*', Route::currentRouteName())) class='active' @endif><a href='painel/contato' title='Contato'>Info de  Contato</a></li>
						<li @if(str_is('painel.contatos_recebidos*', Route::currentRouteName())) class='active' @endif><a href='painel/contatos_recebidos' title='Contatos Recebidos'>Contatos Recebidos</a></li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.(contatos-recebidos|usuarios*)~', Route::currentRouteName())) active @endif ">

					<a href="painel/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários do Painel</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>
