<hr>

<div class="well">

	<label>Imagens</label>

	<div class="multiUpload">
		<div class="icone">
			<span class="glyphicon glyphicon-open"></span>
			<span class="glyphicon glyphicon-refresh"></span>
		</div>
		<p>
			Escolha as imagens da Página. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
			Se preferir também pode utilizar o botão abaixo para selecioná-las.
		</p>
		<input id="fileupload" class="fileupload" type="file" name="files" data-url="painel/imagens/upload" data-path="{{ $path }}" data-fieldname='imagem' data-limite="0" data-fieldname='imagem' multiple>

	</div>

	<div class="limiteImagensAtingido" style='display:none;'>
		<div class="panel panel-default">
			<div class="panel-body bg-info">
		    	Número máximo de imagens (16) atingido nesta página.
		  	</div>
		</div>
	</div>

	<div id="listaImagens">

		@if(isset($registro) && sizeof($registro->imagens))
			@foreach($registro->imagens as $k => $v)
				<div class='projetoImagem'>
        	<img src="assets/images/{{ $path }}/thumbs/{{ $v->imagem }}">
        	<input type='hidden' name='imagem_album[]' value="{{$v->imagem}}">
        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
      	</div>
			@endforeach
		@endif

		@if(isset($imagens) && sizeof($imagens))
			@foreach($imagens as $k => $v)
				<div class='projetoImagem'>
        	<img src="assets/images/{{ $path }}/thumbs/{{ $v->imagem }}">
        	<input type='hidden' name='imagem_album[]' value="{{$v->imagem}}">
        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
      	</div>
			@endforeach
		@endif

	</div>

	<div class="panel panel-default">
		<div class="panel-body">
	    	Você pode clicar e arrastar as imagens para ordená-las.
	  	</div>
	</div>
</div>

<hr>
