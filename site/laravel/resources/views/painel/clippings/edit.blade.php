@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Clipping</h2>

		    <hr>

		    @include('painel.partials.mensagens')

	  	</div>
	</div>

    <form action="{{ URL::route('painel.clippings.update', $registro->id) }}" method="post" enctype="multipart/form-data">

		<input type="hidden" name="_method" value="PUT">

		{!! csrf_field() !!}

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	          	<div class="form-group">
	            	<label for="inputTitulo">Título</label>
	            	<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
	          	</div>

				<div class="form-group">
					@if($registro->capa)
						Capa atual<br>
						<img src="assets/images/clippings/{{$registro->capa}}" class="img-thumbnail"><br>
					@endif
					<label for="inputCapa">Capa</label>
					<input type="file" class="form-control" id="inputCapa" name="capa">
				</div>

				<div class="form-group">
					<label for="inputRevista">Revista</label>
					<input type="text" name="revista" class="form-control" id="inputRevista" value="{{$registro->revista}}">
				</div>

				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" name="data" class="form-control monthpicker" id="inputData" value="{{$registro->data}}">
				</div>

          		@include('painel.partials.imagens-upload', ['path' => 'clippings_imagens'])

			</div>
		</div>

		<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

		<a href="{{ URL::route('painel.clippings.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
