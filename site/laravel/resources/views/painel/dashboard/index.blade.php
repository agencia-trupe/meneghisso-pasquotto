@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      		<h1>Bem Vindo(a),</h1>

  			  <p>{{ date('d/m/Y') }}</p>

  			  <p>Pelo painel administrativo da trupe você pode controlar as principais funções e conteúdo do site.</p>

  			  <p>Qualquer Dúvida entrar em contato: <a href="mailto:contato@trupe.net">contato@trupe.net</a></p>

        </div>
  		</div>
    </div>

@endsection
