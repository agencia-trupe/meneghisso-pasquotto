@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Informações de Contato</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		</div>
	</div>

    <form action="{{ URL::route('painel.contato.update', $registro->id) }}" method="post" enctype="multipart/form-data">

		<input type="hidden" name="_method" value="PUT">

		{!! csrf_field() !!}

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          		<div class="form-group">
          			<label for="inputTexto">Texto</label>
          			<textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{$registro->texto}}</textarea>
          		</div>

				<div class="form-group">
					<label for="inputEmailContato">Email de Contato</label>
					<input type="text" name="email_contato" class="form-control" id="inputEmailContato" value="{{$registro->email_contato}}">
				</div>

				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" name="facebook" class="form-control" id="inputFacebook" value="{{$registro->facebook}}">
				</div>

				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" name="instagram" class="form-control" id="inputInstagram" value="{{$registro->instagram}}">
				</div>

          		<hr>

				<div class="form-group">
					<label for="inputanalytics">Código Analytics</label>
					<input type="text" name="analytics" class="form-control" id="inputanalytics" value="{{$registro->analytics}}">
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Códigos de incorporação do Google Maps</div>
				  	<div class="panel-body">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<label for="inputGoogleMaps">Endereço 1</label>
								</span>
								<input type="text" name="google_maps" class="form-control" id="inputGoogleMaps" value="{{$registro->google_maps}}">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<label for="inputGoogleMapsAlt">Endereço 2</label>
								</span>
								<input type="text" name="google_maps_alt" class="form-control" id="inputGoogleMapsAlt" value="{{$registro->google_maps_alt}}">
							</div>
						</div>
				  	</div>
				</div>

				<hr>

			</div>
		</div>

		<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

		<a href="{{ URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	</form>

</div>

@endsection
