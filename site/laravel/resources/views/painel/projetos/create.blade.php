@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>Cadastrar Projeto</h2>

		        <hr>

		        @include('painel.partials.mensagens')

		   </div>
		</div>

      	<form action="{{ URL::route('painel.projetos.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

					<div class="form-group">
						<label for="inputCategoria">Categoria</label>
						<select class="form-control" name="categoria" required>
							<option value=""></option>
							@forelse($categorias as $categoria)
								<option value="{{$categoria->id}}" @if(old('categoria') == $categoria->id) selected @endif >{{$categoria->titulo}}</option>
							@empty
								<option value="">Sem cadastros</option>
							@endforelse
						</select>
					</div>

					<div class="form-group">
						<label for="inputLocal">Local</label>
						<input type="text" name="local" class="form-control" id="inputLocal" value="{{old('local')}}">
					</div>

            		<div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
					</div>

					<div class="form-group">
						<label for="inputSlug">URL</label>
						<div class="input-group">
							<span class="input-group-addon">projetos/</span>
							<input type="text" name="slug" readonly class="form-control" id="inputSlug" value="{{old('slug')}}">
						</div>						
					</div>

					<div class="form-group">
						<label for="inputMetragem">Metragem</label>
						<div class="input-group" style="max-width: 30%;">
							<input type="text" name="metragem" class="form-control" id="inputMetragem" value="{{old('metragem')}}">
							<span class="input-group-addon">m<sup>2</sup></span>
						</div>
					</div>


  			    	<div class="form-group">
  			    		@if(old('thumbnail'))
  			    			Thumbnail atual<br>
  			    			<img src="assets/images/projetos/{{old('thumbnail')}}" class="img-thumbnail"><br>
  			    		@endif
  			    		<label for="inputThumbnail">Thumbnail</label>
  			    		<input type="file" class="form-control" id="inputThumbnail" name="thumbnail">
  			    	</div>

					@include('painel.partials.imagens-upload', ['path' => 'projetos_imagens'])

            		<hr>

  				</div>
  			</div>

			<button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			<a href="{{ URL::route('painel.projetos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
