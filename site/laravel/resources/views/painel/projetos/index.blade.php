@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Projetos</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.projetos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Projeto</a>

		<hr>

		<div class="btn-group">
			@foreach ($categorias as $key => $cat)
				<a href="{{URL::route('painel.projetos.index', ['filtro' => $cat->id])}}" class="btn btn-sm btn-default @if($cat->id == $filtro) btn-primary @endif ">{{$cat->titulo}}</a>
			@endforeach
			@if($filtro)
				<a href="{{URL::route('painel.projetos.index')}}" class="btn btn-sm btn-link">ver todos</a>
			@endif
		</div>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela='projetos'>

          	<thead>
	          	<tr>
					@if($filtro)
						<th>Ordenar</th>
					@endif
	          		<th>Título</th>
	              	<th>Texto</th>
	          		<th><span class="glyphicon glyphicon-cog"></span></th>
	          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
					@if($filtro)
						<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
					@endif
					<td>
	                  {{$registro->local}}
	                </td>
	                <td>
	                  {{$registro->titulo}}
	                </td>
	            	<td class="crud-actions">
	              		<a href="{{ URL::route('painel.projetos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	                  	<form action="{{ URL::route('painel.projetos.destroy', $registro->id) }}" method="post">
	                    	{!! csrf_field() !!}
	                    	<input type="hidden" name="_method" value="DELETE">
	                    	<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                  	</form>
	            	</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
