@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

      		<h2>Cadastrar Arquiteto</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		</div>
	</div>

    <form action="{{ URL::route('painel.perfil_arquitetos.store') }}" method="post" enctype="multipart/form-data">

  		{!! csrf_field() !!}

  		<div class="row">
  			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            	<div class="form-group">
  					<label for="inputTitulo">Nome</label>
  					<input type="text" name="nome" class="form-control" id="inputTitulo" value="{{old('nome')}}">
  				</div>

  			    <div class="form-group">
  					<label for="inputTexto">Texto</label>
  					<textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{old('texto')}}</textarea>
  				</div>

				<div class="form-group">
					@if(old('imagem'))
						Imagem atual<br>
						<img src="assets/images/perfil_arquitetos/{{old('imagem')}}"><br>
					@endif
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

            	<hr>

  			</div>
  		</div>

		<button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

		<a href="{{ URL::route('painel.perfil_arquitetos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	</form>

</div>

@endsection
