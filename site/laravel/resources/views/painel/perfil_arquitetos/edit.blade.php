@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Arquiteto</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		</div>
	</div>

    <form action="{{ URL::route('painel.perfil_arquitetos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

		<input type="hidden" name="_method" value="PUT">

		{!! csrf_field() !!}

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          		<div class="form-group">
            		<label for="inputTitulo">Nome</label>
            		<input type="text" name="nome" class="form-control" id="inputTitulo" value="{{$registro->nome}}">
          		</div>

			    <div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{$registro->texto}}</textarea>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem atual<br>
						<img src="assets/images/perfil_arquitetos/{{$registro->imagem}}" class="img-thumbnail"><br>
					@endif
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

          		<hr>

			</div>
		</div>

		<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

		<a href="{{ URL::route('painel.perfil_arquitetos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	</form>

</div>

@endsection
