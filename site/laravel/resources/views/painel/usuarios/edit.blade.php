@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

      		<h2>Editar Usuário do Painel Administrativo</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		    <form action="{{ URL::route('painel.usuarios.update', $usuario->id) }}" method="post">

				<input type="hidden" name="_method" value="PUT">

				{!! csrf_field() !!}

			    <div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="name"  value="{{ $usuario->name }}" required>
				</div>

          		<div class="form-group">
					<label for="inputEmail">E-mail</label>
					<input type="email" class="form-control" id="inputEmail" name="email" value="{{ $usuario->email }}">
				</div>

				<div class="form-group">
					<label for="inputSenha">Nova Senha</label>
					<input type="password" class="form-control" id="inputSenha" name="password">
				</div>

				<div class="form-group">
					<label for="inputConfSenha">Digite novamente a Nova Senha</label>
					<input type="password" class="form-control" id="inputConfSenha" name="password_confirm">
				</div>

				<hr>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{ URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</form>
		</div>
	</div>
</div>

@endsection
