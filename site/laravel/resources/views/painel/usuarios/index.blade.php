@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Usuários do Painel Administrativo</h2>

      <hr>

    	@include('painel.partials.mensagens')

      <a href="{{ URL::route('painel.usuarios.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Usuário</a>

      <table class="table table-striped table-bordered table-hover ">

      	<thead>
        	<tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
        @foreach ($usuarios as $usuario)

          <tr class="tr-row">
            <td>{{ $usuario->name }}</td>
            <td>{{ $usuario->email }}</td>
        		<td class="crud-actions">
          		<a href="{{ URL::route('painel.usuarios.edit', $usuario->id ) }}" class="btn btn-primary btn-sm">editar</a>

              @if(Auth::user()->id != $usuario->id)
                <form action="{{ URL::route('painel.usuarios.destroy', $usuario->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              @endif
        		</td>
          </tr>

        @endforeach
        </tbody>

      </table>

    </div>
  </div>
</div>

@endsection
