$(document).ready( function(){

	$('.detalhes-projeto aside').slick({
		vertical: true,
		infinite: false,
		slidesToShow: 3,
	  	slidesToScroll: 1,
		responsive: [
		    {
		      breakpoint: 900,
		      settings: {
		        vertical: false
		      }
		    }
		]
	});

	$('.detalhes-projeto aside a').click( function(e){
		e.preventDefault();
		if (!$(this).hasClass('aberto')) {
			$('.detalhes-projeto aside a.aberto').removeClass('aberto');
			$(this).addClass('aberto');

			var target_href = $(this).attr('href');
			var target_img = $('#ampliada img');

			target_img.fadeOut('normal', function(){
				target_img.attr('src', target_href);
				target_img.fadeIn('normal');
			})
		}
	});

	$('.lista-clippings a').click( function(e){
		e.preventDefault();
		var imgs = JSON.parse($(this).attr('data-images'));
		$.SimpleLightbox.open({
		    items: imgs
		});
	});

	$('#toggle-menu').click( function(e){
		e.preventDefault();
		e.stopPropagation();
		$('nav').addClass('aberto');
	});

	$('nav .menu a').click( function(e){
		e.stopPropagation();
	});

	$(document).click(function() {
    	if ($('nav').hasClass('aberto'))
      		$('nav').removeClass('aberto');
  	});

});
