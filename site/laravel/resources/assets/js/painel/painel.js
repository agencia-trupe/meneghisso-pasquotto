$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

jQuery(function($){

  $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };

  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function busca(termo) {
  $("table").find("tr").each(function(index) {
    if (!index) return;
    var id = $(this).find("td.celula-nome").first().text().toLowerCase();
    $(this).toggle(id.indexOf(termo.toLowerCase()) !== -1);
  });
}

function gerar_slug() {
	var local = $('#inputLocal').val();
	var titulo = $('#inputTitulo').val();
	return slugify(local + ' ' + titulo);
}

function slugify(str) {
	return str.toString().toLowerCase().trim()
	.normalize('NFD')
	.replace(/[\u0300-\u036f]/g, '')
	.replace(/\s+/g, '-')
	.replace(/&/g, '-and-')
	.replace(/[^\w\-]+/g, '')
	.replace(/\-\-+/g, '-');
}

$('document').ready( function(){

  	var e;

  	$("#filtrar-tabela").on("keyup", function() {
    	var a = $(this).val();
    	clearTimeout(e);
    	e = setTimeout(function() {
      		busca(a)
    	}, 300);
  	});

	$('#inputLocal, #inputTitulo').on("keyup", function() {
		$('#inputSlug').val(gerar_slug());
	});


  	// Botão de excluir registro
  	$('.btn-delete').click( function(e){
  		e.preventDefault();
  		var form = $(this).closest('form');
  		bootbox.confirm('Deseja Excluir o Registro?', function(result){
    		if(result)
      			form.submit();
    		else
      			$(this).modal('hide');
  		});
	});

  	// Ordenação na tabela
  	$('table.table-sortable tbody').sortable({
    	update : function () {
      		serial = [];
      		tabela = $('table.table-sortable').attr('data-tabela');
      		$('table.table-sortable tbody').children('tr.tr-row').each(function(idx, elm) {
        	serial.push(elm.id.split('_')[1])
      	});
      	$.post('painel/gravaOrdem', {
        	data : serial,
        	tabela : tabela
      	});
    },
    helper: function(e, ui) {
      	ui.children().each(function() {
        	$(this).width($(this).width());
      	});
      	return ui;
    },
    handle : $('.btn-move'),
    items : '.tr-row'
  	}).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

  	$('.monthpicker').monthpicker({
		prevText: 'Anterior',
		nextText: 'Próximo',
		monthNames: $.datepicker.regional['pt-BR'].monthNames,
		monthNamesShort: $.datepicker.regional['pt-BR'].monthNames
	});

  	if($('textarea').length){
    	$('textarea').not('.textarea-simples').ckeditor({
      		customConfig: '/assets/js/ckeditor_config.js'
    	});
  	}

});
