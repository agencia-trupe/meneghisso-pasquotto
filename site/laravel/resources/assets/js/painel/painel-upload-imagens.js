function iconeUploadStart(){
    $('.multiUpload #icone .glyphicon-open').css('opacity', 0);
    setTimeout( function(){
        $('.multiUpload #icone .glyphicon-refresh').css({'display' : 'block', 'opacity': 1});
        $('.multiUpload #icone .glyphicon-open').css('display', 'none');
    }, 350);
}

function iconeUploadStop(){
    $('.multiUpload #icone .glyphicon-refresh').css('opacity', 0);
    setTimeout( function(){
        $('.multiUpload #icone .glyphicon-open').css({'display' : 'block', 'opacity': 1});
        $('.multiUpload #icone .glyphicon-refresh').css('display', 'none');
    }, 350);
}

$('document').ready( function(){

  $('#fileupload').fileupload({
    dataType: 'json',
    formData: [{
      name  : 'path',
      value : $('#fileupload').attr('data-path')
    }],
    start: function(e, data){
      iconeUploadStart();
    },
    done: function (e, data) {

      if(data.result.envio == 1){

        imagem = "<div class='projetoImagem'>";
          imagem += "<img src='"+data.result.thumb+"'>";
          imagem += "<input type='hidden' name='imagem_album[]' value='"+data.result.filename+"'>";
          imagem += "<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>";
        imagem += "</div>";

        $('#listaImagens').append(imagem);
        $('#listaImagens').sortable("refresh");

      }else{

        var mensagem = "<div class='alert alert-block alert-danger msg-erro-formato' style='font-size:16px'>";
        mensagem += "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        mensagem += (data.result.error_msg == 'extensao_nao_permitida') ? "Extensão não permitida" : "Erro ao enviar a imagem";
        mensagem += "</div>";

        $('#listaImagens').append(mensagem);

        setTimeout(function(){
          $('.msg-erro-formato').fadeOut('normal');
        }, 4000);

      }

      iconeUploadStop();
    }
  });

  $('#listaImagens').sortable().disableSelection();

  $(document).on('click', '.projetoImagem a.btn-remover', function(e){
    e.preventDefault();
    var parent = $(this).parent();
    parent.css('opacity', .35);
    setTimeout( function(){
        parent.remove();
    }, 350);
  });
});
