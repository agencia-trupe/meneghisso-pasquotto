<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'site.home', 'uses' => 'Site\Home\HomeController@getIndex']);

Route::get('perfil/escritorio', ['as' => 'site.perfil.escritorio', 'uses' => 'Site\Perfil\PerfilController@getEscritorio']);
Route::get('perfil/arquitetos', ['as' => 'site.perfil.arquitetos', 'uses' => 'Site\Perfil\PerfilController@getArquitetos']);
Route::get('projetos', ['as' => 'site.projetos', 'uses' => 'Site\Projetos\ProjetosController@getIndex']);
Route::get('projetos/{projeto_slug}', ['as' => 'site.projetos.detalhes', 'uses' => 'Site\Projetos\ProjetosController@getDetalhes']);
Route::get('clippings', ['as' => 'site.clippings', 'uses' => 'Site\Clippings\ClippingsController@getIndex']);
Route::get('contato', ['as' => 'site.contato', 'uses' => 'Site\Contato\ContatoController@getIndex']);
Route::post('contato', ['as' => 'site.contato.enviar', 'uses' => 'Site\Contato\ContatoController@postEnviar']);
// NOVAS ROTAS SITE

Route::get('painel/auth/login',  ['as' => 'painel.login','uses' => 'Painel\Auth\LoginController@getLogin']);
Route::post('painel/auth/login', ['as' => 'painel.auth','uses' => 'Painel\Auth\LoginController@postLogin']);
Route::get('painel/auth/logout', ['as' => 'painel.logout','uses' => 'Painel\Auth\LoginController@getLogout']);

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Painel',
  'prefix' => 'painel'
], function() {

  	Route::get('', ['as' => 'painel.dashboard', 'uses' => function() {
    	return view('painel.dashboard.index');
  	}]);

  	Route::post('gravaOrdem', function(Illuminate\Http\Request $request){
    	$itens = $request->input('data');
    	$tabela = $request->input('tabela');
    	for ($i = 0; $i < count($itens); $i++)
    		DB::table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
  	});

  	Route::resource('usuarios', 'Usuarios\UsuariosController', ['as' => 'painel']);
  	Route::post('imagens/upload', 'Imagens\ImagensController@postUpload');
  	Route::get('contatos_recebidos/export', ['as' => 'painel.contatos_recebidos.export', 'uses' => 'ContatosRecebidos\ContatosRecebidosController@export']);

  	// NOVAS ROTAS DO PAINEL:
	Route::resource('banners', 'Banners\BannersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
	Route::resource('perfil_escritorio', 'PerfilEscritorio\PerfilEscritorioController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
	Route::resource('perfil_arquitetos', 'PerfilArquitetos\PerfilArquitetosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
	Route::resource('categorias', 'Categorias\CategoriasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
	Route::resource('projetos', 'Projetos\ProjetosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
	Route::resource('clippings', 'Clippings\ClippingsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'], 'as' => 'painel']);
	Route::resource('contato', 'Contato\ContatoController', ['only' => ['index', 'edit', 'update'], 'as' => 'painel']);
	Route::resource('contatos_recebidos', 'ContatosRecebidos\ContatosRecebidosController', ['only' => ['index', 'show', 'destroy'], 'as' => 'painel']);

});
