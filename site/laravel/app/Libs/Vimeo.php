<?phpDfs

namespace App\Libs;

use \Image, \Input;

/**
*	Classe para manipulação de url do VIMEO
*	@author Bruno Monteiro
*	@since 06/08/2015
*/

class Vimeo {

	private $url;
	private $id;

	private $oembed_endpoint = 'http://vimeo.com/api/oembed';
	private $thumb_path = 'assets/img/portfolio/videos/';
	private $servico = 'vimeo';

	public function __construct($url)
	{
		$this->url = $url;
		$this->id = $this->setId();
	}

	public function getUrl(){ return $this->url; }
	public function getServico(){ return $this->servico; }

	public function getEmbed($autoplay = 0, $largura = null, $altura = null)
	{
		/*
		*  default_ratio = 16/9;
		*/
		$dimensoes_str = "";

		if($largura != null || $altura != null){

			if($largura != null && $altura === null)
				$altura = (9 * $largura)/16;

			if($largura === null && $altura != null)
				$largura = (16 * $altura)/9;

			$dimensoes_str = " width='".$largura."' height='".$altura."'";
		}

		return "<iframe src=\"//player.vimeo.com/video/{$this->id}?autoplay=".$autoplay."&loop=1&title=0&byline=0&portrait=0\" {$dimensoes_str} frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
	}

	public function getThumbnail($tamanho = 'large', $salvar = true) {
		// @tamanho: 'small', 'medium', 'large'

		$tamanhos = ['small', 'medium', 'large'];

		if(!in_array($tamanho, $tamanhos))
			$tamanho = 'large';

		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/{$this->id}.php"));
		$url_thumb = isset($hash[0]['thumbnail_'.$tamanho]) ? $hash[0]['thumbnail_'.$tamanho] : false;

		if($url_thumb === false)
			return '';

		$filename = $this->id.'.jpg';

		if($salvar){

			if(!file_exists(public_path($this->thumb_path)))
				mkdir(public_path($this->thumb_path, 0777));

			copy($url_thumb, $this->thumb_path.'/'.$filename);
		}

		return $filename;
	}

	public function setId()
	{
		$json_url = $this->oembed_endpoint . '.json?url=' . rawurlencode($this->url) . '&width=640';

		$oembed = json_decode($this->_curl_get($json_url));

		return isset($oembed->video_id) ? $oembed->video_id : 0;
	}

	public function getId()
	{
		return $this->id;
	}

	private function _curl_get($url) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		$return = curl_exec($curl);
		curl_close($curl);
		return $return;
	}

}
