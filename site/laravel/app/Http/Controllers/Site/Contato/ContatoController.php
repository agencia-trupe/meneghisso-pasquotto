<?php

namespace App\Http\Controllers\Site\Contato;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use Mail;

class ContatoController extends Controller
{

  	public function getIndex(Request $request)
  	{
    	return view('site.contato.index');
  	}

  	public function postEnviar(Request $request)
  	{
		$this->validate($request, [
	    	'nome' => 'required',
	    	'email' => 'required|email',
	    	'mensagem' => 'required'
	    ]);

		$data['contato'] = Contato::first();
	    $data['nome'] = $request->nome;
	    $data['email'] = $request->email;
	    $data['mensagem'] = $request->mensagem;

	    Mail::send('emails.contato', $data, function($message) use ($data)
	    {
	      $message->to($data['contato']->email_contato)
	              ->subject('Novo contato - '.$data['nome'])
	              ->replyTo($data['email'], $data['nome']);
	    });

	    $novo = ContatoRecebido::create($data);		

	    $request->session()->flash('contato_enviado', true);

	    return redirect()->back();
  	}

}
