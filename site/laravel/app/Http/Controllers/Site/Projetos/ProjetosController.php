<?php

namespace App\Http\Controllers\Site\Projetos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{

  	public function getIndex(Request $request)
  	{
		$categorias = Categoria::ordenado()->get();

		$filtro = $request->categoria ? $this->getCategoryIdFromSlug($request->categoria) : $categorias->first()->id;

		/*
			Filtragem por categorias deprecada de acordo com solicitação do cliente
			Vou manter o código aqui caso precise voltar eventualmente

			//$projetos = Projeto::ordenado()->filtro($filtro)->get();
		*/
		$projetos = Projeto::ordenado()->get();

    	return view('site.projetos.index', [
			'categorias' => [],
			'projetos' => $projetos,
			'filtro' => $filtro
		]);
  	}

	private function getCategoryIdFromSlug($categoria_slug)
	{
		$cat = Categoria::where('slug', $categoria_slug)->first();

		if(!$cat)
			return Categoria::ordenado()->first()->id;
		else
			return $cat->id;
	}

	public function getDetalhes(Request $request, $slug)
	{
		$projeto = Projeto::where('slug', $slug)->with('imagens')->first();

		if (!$projeto)
			abort('404');

		return view('site.projetos.detalhes', [
			'projeto' => $projeto
		]);
	}

}
