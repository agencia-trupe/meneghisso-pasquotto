<?php

namespace App\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;

class HomeController extends Controller
{

  	public function getIndex(Request $request)
  	{
		$banners = Banner::ordenado()->get();

    	return view('site.home.index', [
			'banners' => $banners
		]);
  	}

}
