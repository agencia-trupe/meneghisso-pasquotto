<?php

namespace App\Http\Controllers\Site\Perfil;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PerfilEscritorio;
use App\Models\PerfilArquitetos;

class PerfilController extends Controller
{

	public function getEscritorio(Request $request)
	{
		$texto = PerfilEscritorio::first();

		return view('site.perfil.escritorio', [
			'texto' => $texto
		]);
	}

  	public function getArquitetos(Request $request)
  	{
		$arquitetos = PerfilArquitetos::ordenado()->get();

		return view('site.perfil.arquitetos', [
			'arquitetos' => $arquitetos
		]);
  	}

}
