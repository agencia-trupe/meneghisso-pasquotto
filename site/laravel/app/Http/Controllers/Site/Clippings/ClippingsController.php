<?php

namespace App\Http\Controllers\Site\Clippings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Clipping;

class ClippingsController extends Controller
{

  	public function getIndex(Request $request)
  	{
	  	$clippings = Clipping::ordenado()->with('imagens')->get();

    	return view('site.clippings.index', [
			'clippings' => $clippings
		]);
  	}

}
