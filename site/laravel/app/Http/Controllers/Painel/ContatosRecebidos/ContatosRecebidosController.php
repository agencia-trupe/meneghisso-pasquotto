<?php

namespace App\Http\Controllers\Painel\ContatosRecebidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;
use App\Libs\Thumbs;
use League\Csv\Writer;

class ContatosRecebidosController extends Controller
{
    protected $pathImagens = 'contatos_recebidos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = ContatoRecebido::ordenado()->get();

      return view('painel.contatos_recebidos.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
      return view('painel.contatos_recebidos.show')->with('registro', ContatoRecebido::find($id));
    }

	public function destroy(Request $request, $id)
    {
		$object = ContatoRecebido::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Registro removido com sucesso.');

		return redirect()->route('painel.contatos_recebidos.index');
    }

    public function export(Request $request)
    {
		$contatos = ContatoRecebido::ordenado()->get();

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(['Nome', 'E-mail', 'Mensagem', 'Data']);

        foreach ($contatos as $contato) {
            $csv->insertOne([
				'nome' => $contato->nome,
				'email' => $contato->email,
				'mensagem' => $contato->mensagem,
				'data' => $contato->created_at->format('d/m/Y H:i:s')
			]);
        }

        $csv->output('contatos_recebidos_'.date('d_m_Y_H_i').'.csv');
    }

}
