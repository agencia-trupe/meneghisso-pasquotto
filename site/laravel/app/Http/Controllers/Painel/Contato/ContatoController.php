<?php

namespace App\Http\Controllers\Painel\Contato;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Libs\Thumbs;

class ContatoController extends Controller
{
    protected $pathImagens = 'contato/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      	$registros = Contato::all();

      	return view('painel.contato.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      	return view('painel.contato.edit')->with('registro', Contato::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
      		'email_contato' => 'required|email'
    	]);

      	$object = Contato::find($id);

		$object->texto = $request->texto;
		$object->email_contato = $request->email_contato;
		$object->analytics = $request->analytics;
		$object->facebook = $request->facebook;
		$object->instagram = $request->instagram;
		$object->google_maps = $request->google_maps;
		$object->google_maps_alt = $request->google_maps_alt;


      	try {

        	$object->save();

        	$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        	return redirect()->route('painel.contato.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
