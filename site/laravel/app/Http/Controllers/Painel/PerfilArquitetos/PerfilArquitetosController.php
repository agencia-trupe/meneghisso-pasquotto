<?php

namespace App\Http\Controllers\Painel\PerfilArquitetos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PerfilArquitetos;
use App\Libs\Thumbs;

class PerfilArquitetosController extends Controller
{
    protected $pathImagens = 'perfil_arquitetos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = PerfilArquitetos::ordenado()->get();

      return view('painel.perfil_arquitetos.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.perfil_arquitetos.create');
    }

    public function store(Request $request)
    {
      	$this->validate($request, [
      		'nome' => 'required'
    	]);

      	$object = new PerfilArquitetos;

      	$object->nome = $request->nome;
      	$object->texto = $request->texto;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'imagem', 230, 300, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/', $dt);
			$object->imagem = $imagem;
		}

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.perfil_arquitetos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.perfil_arquitetos.edit')->with('registro', PerfilArquitetos::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
      		'nome' => 'required'
    	]);

      	$object = PerfilArquitetos::find($id);

      	$object->nome = $request->nome;
      	$object->texto = $request->texto;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'imagem', 230, 300, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/', $dt);
			$object->imagem = $imagem;
		}

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.perfil_arquitetos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = PerfilArquitetos::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.perfil_arquitetos.index');
    }

}
