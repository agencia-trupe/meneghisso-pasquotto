<?php

namespace App\Http\Controllers\Painel\PerfilEscritorio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PerfilEscritorio;
use App\Libs\Thumbs;

class PerfilEscritorioController extends Controller
{
    protected $pathImagens = 'perfil_escritorio/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = PerfilEscritorio::all();

      return view('painel.perfil_escritorio.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.perfil_escritorio.edit')->with('registro', PerfilEscritorio::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = PerfilEscritorio::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

	  $dt = date('dmYHis');
	  $imagem = Thumbs::make($request, 'imagem', 600, null, $this->pathImagens, $dt);
	  if($imagem){
	  	Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/', $dt);
	  	$object->imagem = $imagem;
	  }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.perfil_escritorio.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
