<?php

namespace App\Http\Controllers\Painel\Projetos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoImagem;
use App\Models\Categoria;
use App\Libs\Thumbs;

class ProjetosController extends Controller
{
    protected $pathImagens = 'projetos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
		$filtro = $request->get('filtro');
		$categorias = Categoria::ordenado()->get();
		$registros = Projeto::ordenado()->filtro($filtro)->get();

      	return view('painel.projetos.index')->with([
			'filtro' => $filtro,
			'categorias' => $categorias,
			'registros' => $registros
		]);
    }

    public function create(Request $request)
    {
		$categorias = Categoria::ordenado()->get();
      	return view('painel.projetos.create')->with(compact('categorias'));
    }

    public function store(Request $request)
    {
      	$this->validate($request, [
			'categoria' => 'required|exists:categorias,id',
      		'titulo' => 'required',
			'thumbnail' => 'required|image',
			'slug' => 'required|unique:projetos,slug'
    	]);

      	$object = new Projeto;

		$object->categoria = $request->categoria;
		$object->local = $request->local;
      	$object->titulo = $request->titulo;
		$object->slug = $request->slug;
      	$object->metragem = $request->metragem;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'thumbnail', 400, null, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'thumbnail', 300, 300, $this->pathImagens.'thumbs/', $dt);
			$object->thumbnail = $imagem;
		}

      	try {

        	$object->save();

			$imagens_albuns = $request->imagem_album;
	        if(sizeof($imagens_albuns)){
	          foreach ($imagens_albuns as $ordem => $img) {
	            $object->imagens()->save( new ProjetoImagem([
	                'imagem' => $img,
	                'ordem' => $ordem
	              ])
	            );
	          }
	        }

        	$request->session()->flash('sucesso', 'Registro criado com sucesso.');

        	return redirect()->route('painel.projetos.index', ['filtro' => $object->categoria]);

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      	}
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$categorias = Categoria::ordenado()->get();
      	return view('painel.projetos.edit')->with('registro', Projeto::find($id))
										   ->with(compact('categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request, [
			'categoria' => 'required|exists:categorias,id',
      		'titulo' => 'required',
			'thumbnail' => 'sometimes|image',
			'slug' => 'required|unique:projetos,slug,'.$id
    	]);

      	$object = Projeto::find($id);

		$object->categoria = $request->categoria;
		$object->local = $request->local;
      	$object->titulo = $request->titulo;
		$object->slug = $request->slug;
      	$object->metragem = $request->metragem;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'thumbnail', 400, null, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'thumbnail', 300, 300, $this->pathImagens.'thumbs/', $dt);
			$object->thumbnail = $imagem;
		}

      	try {

        	$object->save();

			$imgs_atuais = $object->imagens;
	        foreach ($imgs_atuais as $img_atual)
	          $img_atual->delete();

	        $imagens_albuns = $request->imagem_album;
	        if(sizeof($imagens_albuns)){
	          foreach ($imagens_albuns as $ordem => $img) {
	            $object->imagens()->save( new ProjetoImagem([
	                'imagem' => $img,
	                'ordem' => $ordem
	              ])
	            );
	          }
	        }

        	$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        	return redirect()->route('painel.projetos.index', ['filtro' => $object->categoria]);

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      	}
    }

    public function destroy(Request $request, $id){
      	$object = Projeto::find($id);
		$cat = $object->categoria;
      	$object->delete();

      	$request->session()->flash('sucesso', 'Registro removido com sucesso.');

      	return redirect()->route('painel.projetos.index', ['filtro' => $cat]);
    }

}
