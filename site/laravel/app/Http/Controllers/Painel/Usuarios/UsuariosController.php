<?php

namespace App\Http\Controllers\Painel\Usuarios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsuariosController extends Controller
{

	public function index()
	{
		$usuarios = User::all();

		return view('painel.usuarios.index')->with(compact('usuarios'));
	}

	public function create()
	{
		return view('painel.usuarios.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
      		'name'             => 'required',
      		'email'            => 'required|unique:users,email',
      		'password'         => 'required|min:6',
      		'password_confirm' => 'required|min:6|same:password',
    	]);

		$object = new User;

		$object->name     = $request->name;
    	$object->email    = $request->email;
		$object->password = bcrypt($request->password);

		try {
			$object->save();
			$request->session()->flash('sucesso', 'Usuário criado com sucesso.');
			return redirect()->route('painel.usuarios.index');
		} catch (\Exception $e) {
			$request->flash();
			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));
		}
	}

	public function edit($id)
	{
		$usuario = User::find($id);
		return view('painel.usuarios.edit')->with(compact('usuario'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
      		'name'             => 'required',
      		'email'            => 'required|unique:users,email,'.$id,
      		'password'         => 'sometimes|min:6',
      		'password_confirm' => 'required_with:password|min:6|same:password',
    	]);

		$object = User::find($id);

		$object->name = $request->name;
    	$object->email = $request->email;

		if($request->has('password'))
			$object->password = bcrypt($request->password);

		try {
			$object->save();
			$request->session()->flash('sucesso', 'Usuário alterado com sucesso.');
			return redirect()->route('painel.usuarios.index');
		} catch (\Exception $e) {
			$request->flash();
			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));
		}
	}

	public function destroy(Request $request, $id)
	{
		$object = User::find($id);
		$object->delete();
		$request->session()->flash('sucesso', 'Usuário removido com sucesso.');

		return redirect()->route('painel.usuarios.index');
	}
}
