<?php

namespace App\Http\Controllers\Painel\Imagens;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Libs\Thumbs;

class ImagensController extends Controller
{

	  /*
	    Armazena as imagens enviadas via Ajax
	  */
  	public function postUpload(Request $request)
  	{
	    $arquivo = $request->file('files');
	    $path    = $request->input('path');

	    $path_original = "assets/images/{$path}/";
	    $path_thumb    = "assets/images/{$path}/thumbs/";
		$path_thumb_quadradas = "assets/images/{$path}/thumbs-quadradas/";

		$path_upload   = public_path($path_original);

		$nome_arquivo = $arquivo->getClientOriginalName();
	    $extensao_arquivo = $arquivo->getClientOriginalExtension();
	    $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

	    $filename = date('YmdHis').str_slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

	    $name = $path_original.$filename;
	    $thumb = $path_thumb_quadradas.$filename;

	    // redimensionadas
	    $arquivo->move($path_upload, $filename);
	    Thumbs::makeFromFile($path_upload, $filename, 1200, null, public_path($path_original), 'rgba(255,255,255,1)', false);
	    Thumbs::makeFromFile($path_upload, $filename, 300, null, public_path($path_thumb), 'rgba(255,255,255,1)', false);
		Thumbs::makeFromFile($path_upload, $filename, 300, 300, public_path($path_thumb_quadradas));

	    return [
	      	'envio' => 1,
	      	'thumb' => $thumb,
	      	'filename' => $filename
	    ];
  	}

}
