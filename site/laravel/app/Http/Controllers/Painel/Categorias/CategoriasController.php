<?php

namespace App\Http\Controllers\Painel\Categorias;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categoria;
use App\Libs\Thumbs;

class CategoriasController extends Controller
{
    protected $pathImagens = 'categorias/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Categoria::ordenado()->get();

      return view('painel.categorias.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.categorias.create');
    }

    public function store(Request $request)
    {
      	$this->validate($request, [
      		'titulo' => 'required|unique:categorias'
    	]);

      	$object = new Categoria;

      	$object->titulo = $request->titulo;

      	try {

        	$object->save();

        	$request->session()->flash('sucesso', 'Registro criado com sucesso.');

        	return redirect()->route('painel.categorias.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.categorias.edit')->with('registro', Categoria::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
      		'titulo' => 'required|unique:categorias,titulo,'.$id
    	]);

      	$object = Categoria::find($id);

      	$object->titulo = $request->titulo;

      	try {

        	$object->save();

        	$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        	return redirect()->route('painel.categorias.index');

      	} catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Categoria::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.categorias.index');
    }

}
