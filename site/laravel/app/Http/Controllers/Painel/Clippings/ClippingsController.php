<?php

namespace App\Http\Controllers\Painel\Clippings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagem;
use App\Libs\Thumbs;

class ClippingsController extends Controller
{
    protected $pathImagens = 'clippings/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      	$registros = Clipping::ordenado()->get();

      	return view('painel.clippings.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      	return view('painel.clippings.create');
    }

    public function store(Request $request)
    {
      	$this->validate($request, [
      		'titulo' => 'required',
			'capa' => 'required|image',
			'data' => 'required|date_format:m/Y'
    	]);

      	$object = new Clipping;

      	$object->titulo = $request->titulo;
      	$object->revista = $request->revista;
		$object->data = $request->data;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'capa', 200, 285, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'capa', 200, 200, $this->pathImagens.'thumbs/', $dt);
			$object->capa = $imagem;
		}

      	try {

        	$object->save();

			$imagens_albuns = $request->imagem_album;
	        if(sizeof($imagens_albuns)){
	          foreach ($imagens_albuns as $ordem => $img) {
	            $object->imagens()->save( new ClippingImagem([
	                'imagem' => $img,
	                'ordem' => $ordem
	              ])
	            );
	          }
	        }

        	$request->session()->flash('sucesso', 'Registro criado com sucesso.');

        	return redirect()->route('painel.clippings.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      	}
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      	return view('painel.clippings.edit')->with('registro', Clipping::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
			'titulo' => 'required',
			'capa' => 'sometimes|image',
			'data' => 'required|date_format:m/Y'
    	]);

      	$object = Clipping::find($id);

		$object->titulo = $request->titulo;
      	$object->revista = $request->revista;
		$object->data = $request->data;

		$dt = date('dmYHis');
		$imagem = Thumbs::make($request, 'capa', 200, 285, $this->pathImagens, $dt);
		if($imagem){
			Thumbs::make($request, 'capa', 200, 200, $this->pathImagens.'thumbs/', $dt);
			$object->capa = $imagem;
		}

      	try {

        	$object->save();

			$imgs_atuais = $object->imagens;
	        foreach ($imgs_atuais as $img_atual)
	          $img_atual->delete();

	        $imagens_albuns = $request->imagem_album;
	        if(sizeof($imagens_albuns)){
	          foreach ($imagens_albuns as $ordem => $img) {
	            $object->imagens()->save( new ClippingImagem([
	                'imagem' => $img,
	                'ordem' => $ordem
	              ])
	            );
	          }
	        }

        	$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        	return redirect()->route('painel.clippings.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      	}
    }

    public function destroy(Request $request, $id){
      	$object = Clipping::find($id);
      	$object->delete();

      	$request->session()->flash('sucesso', 'Registro removido com sucesso.');

      	return redirect()->route('painel.clippings.index');
    }

}
