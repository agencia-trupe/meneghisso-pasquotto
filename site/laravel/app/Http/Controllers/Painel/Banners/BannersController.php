<?php

namespace App\Http\Controllers\Painel\Banners;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Libs\Thumbs;

class BannersController extends Controller
{

    protected $pathImagens = 'banners/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      	$registros = Banner::ordenado()->get();

      	return view('painel.banners.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      	return view('painel.banners.create');
    }

    public function store(Request $request)
    {
      	$this->validate($request, [
      		'imagem' => 'required|image'
    	]);

      	$object = new Banner;

		$dt = date('dmYHis');
      	$imagem = Thumbs::make($request, 'imagem', 2000, null, $this->pathImagens, $dt);
      	if($imagem){
      		Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/', $dt);
      		$object->imagem = $imagem;
      	}

      	try {

        	$object->save();

        	$request->session()->flash('sucesso', 'Registro criado com sucesso.');

        	return redirect()->route('painel.banners.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      	}
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      	return view('painel.banners.edit')->with('registro', Banner::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
      		'imagem' => 'sometimes|image'
    	]);

      	$object = Banner::find($id);

		$dt = date('dmYHis');
	  	$imagem = Thumbs::make($request, 'imagem', 2000, null, $this->pathImagens, $dt);
      	if($imagem){
      		Thumbs::make($request, 'imagem', 200, 200, $this->pathImagens.'thumbs/', $dt);
      		$object->imagem = $imagem;
      	}

      	try {

        	$object->save();

        	$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        	return redirect()->route('painel.banners.index');

      	} catch (\Exception $e) {

        	$request->flash();

        	return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      	}
    }

    public function destroy(Request $request, $id){
      	$object = Banner::find($id);
      	$object->delete();

      	$request->session()->flash('sucesso', 'Registro removido com sucesso.');

      	return redirect()->route('painel.banners.index');
    }

}
