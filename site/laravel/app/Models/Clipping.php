<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Clipping extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  	protected $table = 'clippings';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  	protected $fillable = [
    	'',
  	];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  	protected $hidden = ['id'];

	public function setDataAttribute($value)
	{
		list($mes, $ano) = explode('/', $value);
    	$this->attributes['data'] = $ano.'-'.$mes.'-01';
	}

	public function getDataAttribute($value)
  	{
    	return Carbon::createFromFormat('Y-m-d', $value)->format('m/Y');
  	}

	public function imagens()
  	{
    	return $this->hasMany('App\Models\ClippingImagem', 'clipping')->ordenado();
  	}

  	public function scopeOrdenado($query)
  	{
    	return $query->orderBy('data', 'desc');
  	}
}
