<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  	protected $table = 'categorias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  	protected $fillable = [
    	'',
  	];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  	protected $hidden = ['id'];

  	public static function boot()
  	{
	  	parent::boot();

	  	self::saving(function($model){
			$model->slug = str_slug($model->titulo);
	  	});

	  	self::creating(function($model){
			$model->ordem = count(Categoria::all()) + 1;
	  	});

  	}	

  	public function scopeOrdenado($query)
  	{
    	return $query->orderBy('ordem', 'asc');
  	}
}
