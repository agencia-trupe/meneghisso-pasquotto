<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoRecebido extends Model
{

    /**
    * The database table used by the model.
    *
    * @var string
    */
  	protected $table = 'contatos_recebidos';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
  	protected $fillable = [
    	'nome',
		'email',
		'mensagem'
   	];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
  	protected $hidden = ['id', 'updated_at'];

	protected $dates = ['created_at', 'updated_at'];

	public function scopeOrdenado($query)
  	{
    	return $query->orderBy('created_at', 'desc');
  	}
}
