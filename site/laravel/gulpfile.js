var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

var paths = {
	src : {
		less : 'resources/assets/less/',
		js : 'resources/assets/js/',
		vendor : 'node_modules/',
		fonts : 'resources/assets/fonts/'
	},
	output : {
		less : '../public_html/assets/css/',
		js : '../public_html/assets/js/',
		fonts : '../public_html/assets/css/fonts/',
		img : '../public_html/assets/images/',
		vendor: '../public_html/assets/vendor/'
	},
};

elixir(function(mix) {

  mix

    .less([
		paths.src.less + 'template.less',
		paths.src.less + 'header.less',
		paths.src.less + 'home.less',
		paths.src.less + 'perfil.less',
		paths.src.less + 'projetos.less',
		paths.src.less + 'clippings.less',
		paths.src.less + 'contato.less'
	], paths.src.less + 'build.css')

    .styles([
      	paths.src.vendor + 'css-reset/reset.css',
	  	paths.src.vendor + 'slick-carousel/slick/slick.css',
	  	paths.src.less   + 'slick-theme.css',
	  	paths.src.vendor + 'simple-lightbox/dist/simpleLightbox.css',
      	paths.src.less   + 'build.css'
    ], paths.output.less + 'main.css', '.')

	.copy([
		paths.src.vendor + 'jquery/dist/jquery.js'
	], paths.output.js)

	.scripts([
		paths.src.vendor + 'jquery-cycle-2/src/jquery.cycle.all.js',
		paths.src.vendor + 'slick-carousel/slick/slick.js',
		paths.src.vendor + 'simple-lightbox/dist/simpleLightbox.js',
		paths.src.js + 'site.js'
	], paths.output.js + 'site.js', './')


	// PAINEL
	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.output.vendor + 'jquery.js', './')
  	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.output.vendor + 'bootstrap.js', './')
    .copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.output.fonts)
    .copy( paths.src.vendor + 'ckeditor/', paths.output.vendor + 'ckeditor/')
	.copy( paths.src.vendor + 'jquery-ui-dist/images', paths.output.less + 'painel/images/')
	//.copy( paths.src.vendor + 'ckeditor/config.js', paths.src.js + 'painel/ckeditor_config.js')

	.less([
    	paths.src.less + 'painel.less'
    ], paths.src.less + 'build_painel.css', './')

    // ESTILOS
  	.styles([
  	  	paths.src.vendor + 'css-reset/reset.css',
		paths.src.vendor + 'jquery-ui-dist/jquery-ui.css',
		paths.src.less   + 'flatly.bootstrap.min.css',
		paths.src.less   + 'build_painel.css',
  	], paths.output.less + 'painel/painel.css', './')

    // SCRIPTS
    .scripts( paths.src.js + 'painel/ckeditor_config.js', paths.output.js + 'ckeditor_config.js', './')

    .scripts([
      	paths.src.vendor + 'jquery-ui-dist/jquery-ui.min.js',
      	paths.src.vendor + 'bootbox/bootbox.js',
      	paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
      	paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
		paths.src.vendor + 'jquery-ui-monthpicker/jquery.ui.monthpicker.js',
		paths.src.js	 + 'painel/painel-upload-imagens.js',
      	paths.src.js     + 'painel/painel.js'
    ], paths.output.js  + 'painel.js', './');

});
