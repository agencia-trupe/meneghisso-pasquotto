<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInfoContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->text('google_maps_alt')->nullable()->after('google_maps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->dropColumn('google_maps_alt');
        });
    }
}
/*
alter table `contato` add `google_maps_alt` text null after `google_maps`
