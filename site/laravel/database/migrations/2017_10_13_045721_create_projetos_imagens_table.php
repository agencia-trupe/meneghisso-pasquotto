<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projetos_imagens', function (Blueprint $table) {
            $table->increments('id');

			$table->integer('projeto')->unsigned();
			$table->foreign('projeto')->references('id')->on('projetos')->onDelete('cascade');

			$table->string('imagem');
			$table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projetos_imagens');
    }
}
