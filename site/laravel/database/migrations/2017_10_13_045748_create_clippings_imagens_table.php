<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingsImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clippings_imagens', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('clipping')->unsigned();
			$table->foreign('clipping')->references('id')->on('clippings')->onDelete('cascade');

			$table->string('imagem');
			$table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clippings_imagens');
    }
}
