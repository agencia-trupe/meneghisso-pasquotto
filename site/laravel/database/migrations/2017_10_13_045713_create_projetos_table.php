<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->increments('id');

			$table->integer('categoria')->unsigned();
			$table->foreign('categoria')->references('id')->on('categorias')->onDelete('cascade');

			$table->string('local')->nullable();
			$table->string('titulo')->nullable();
			$table->string('metragem')->nullable();
			$table->string('thumbnail')->nullable();
			$table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projetos');
    }
}
