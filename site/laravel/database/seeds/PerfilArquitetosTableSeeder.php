<?php

use Illuminate\Database\Seeder;

class PerfilArquitetosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('perfil_arquitetos')->delete();
		DB::table('perfil_arquitetos')->insert([
			[
				'nome' => '<p>Arquiteto 1</p>',
				'ordem' => 0
			],
			[
				'nome' => '<p>Arquiteto 2</p>',
				'ordem' => 1
			]
		]);
    }
}
