<?php

use Illuminate\Database\Seeder;

class PerfilEscritorioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfil_escritorio')->delete();
		DB::table('perfil_escritorio')->insert([
			'titulo' => '<p>Perfil Escritorio</p>'
		]);
    }
}
