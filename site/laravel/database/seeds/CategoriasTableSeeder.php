<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('categorias')->delete();
		DB::table('categorias')->insert([
			[
				'titulo' => 'Residenciais',
				'slug' => 'residenciais',
				'ordem' => 0
			],
			[
				'titulo' => 'Comerciais',
				'slug' => 'comerciais',
				'ordem' => 1
			],
			[
				'titulo' => 'Corporativos',
				'slug' => 'corporativos',
				'ordem' => 2
			]
		]);
    }
}
